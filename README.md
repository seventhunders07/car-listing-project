# Car Listing

This is the repository for all source codes of car listing project.

### App Specs
- minSdkVersion 16
- targetSdkVersion 27
- compileSdkVersion 27

### Environment Used
- Android Studio 3.0
- Gradle 3.0.1


### Libraries Used
- [Butterknife] (http://jakewharton.github.io/butterknife/) 
- [GSON] (https://github.com/google/gson)
- [Retrofit] (http://square.github.io/retrofit/)
- [Glide] (https://github.com/bumptech/glide)
- [ExpandableTextView] (https://github.com/Blogcat/Android-ExpandableTextView)
- [ObjectBox] (https://github.com/objectbox/objectbox-java)
- [ThirtyInch] (https://github.com/grandcentrix/ThirtyInch)
