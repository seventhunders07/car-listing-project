package com.carmudi.carmudiapp;

import android.app.Application;
import android.content.Context;

import com.carmudi.carmudiapp.model.MyObjectBox;

import io.objectbox.BoxStore;

/**
 * Created by root on 2/5/18.
 */

public class BaseApplication extends Application {

    static BaseApplication mInstance;
    BoxStore mBoxStore;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        initializeObjectBox();
    }

    public Context getContext(){
        return getApplicationContext();
    }

    public static BaseApplication getInstance(){
        return mInstance;
    }

    public void initializeObjectBox(){
        mBoxStore = MyObjectBox.builder().androidContext(this).build();
    }

    public BoxStore getBoxStore(){
        return mBoxStore;
    }
}
