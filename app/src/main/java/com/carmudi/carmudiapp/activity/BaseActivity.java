package com.carmudi.carmudiapp.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;

import com.carmudi.carmudiapp.presenter.BasePresenter;
import com.carmudi.carmudiapp.view.BaseView;
import com.pascalwelsch.compositeandroid.activity.CompositeActivity;

import net.grandcentrix.thirtyinch.TiActivity;
import net.grandcentrix.thirtyinch.internal.TiPresenterProvider;
import net.grandcentrix.thirtyinch.plugin.TiActivityPlugin;

import butterknife.ButterKnife;

/**
 * Created by root on 2/5/18.
 */

public abstract class BaseActivity extends CompositeActivity implements BaseView {

    private BasePresenter mBasePresenter;
    protected abstract BasePresenter injectPresenter();
    public Context mContext;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(setLayoutResource());
        mContext = this;
        ButterKnife.bind(this);
    }

    public BaseActivity() {
        addPlugin(new TiActivityPlugin<>(
                new TiPresenterProvider<BasePresenter>() {
                    @NonNull
                    @Override
                    public BasePresenter providePresenter() {
                        mBasePresenter = injectPresenter();
                        return mBasePresenter;
                    }
                }));
    }

    public <T> T getPresenter(Class<T> parsingClass) {
        return (parsingClass.cast(mBasePresenter));
    }

    public abstract int setLayoutResource();

    public void setToolbar(boolean canGoBack, Toolbar toolbar){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(canGoBack);
    }


}
