package com.carmudi.carmudiapp.activity;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.LinearLayout;

import com.carmudi.carmudiapp.R;
import com.carmudi.carmudiapp.adapter.FullDetailAdapter;
import com.carmudi.carmudiapp.model.Attributes_;
import com.carmudi.carmudiapp.model.CarDataResponse;
import com.carmudi.carmudiapp.model.Detail;
import com.carmudi.carmudiapp.model.Image;
import com.carmudi.carmudiapp.model.Optional;
import com.carmudi.carmudiapp.model.Result;
import com.carmudi.carmudiapp.presenter.BasePresenter;
import com.carmudi.carmudiapp.presenter.CarDataPresenter;
import com.carmudi.carmudiapp.utils.AppUtils;
import com.carmudi.carmudiapp.utils.ImageLoader;
import com.carmudi.carmudiapp.view.CarDataView;
import com.mzelzoghbi.zgallery.ZGallery;
import com.mzelzoghbi.zgallery.entities.ZColor;

import java.util.ArrayList;
import java.util.List;

import at.blogc.android.views.ExpandableTextView;
import butterknife.BindView;
import butterknife.OnClick;

public class CarDetailsActivity extends BaseActivity {

    public static String SERIALIZED_RESULT_KEY = "serializedResult";

    @BindView(R.id.fullImageView)
    AppCompatImageView mFullImageView;

    @BindView(R.id.carTitleTextView)
    AppCompatTextView mCarTitleTextView;

    @BindView(R.id.priceTextView)
    AppCompatTextView mPriceTextView;

    @BindView(R.id.priceConditionsTextView)
    AppCompatTextView mPriceConditionTextView;

    @BindView(R.id.activatedAtTextView)
    AppCompatTextView mActivateAtTextView;

    @BindView(R.id.RVHeaderList)
    RecyclerView mRVHeaderList;

    @BindView(R.id.RVValueList)
    RecyclerView mRVValueList;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.mileageValueTextView)
    AppCompatTextView mMileageValueTextView;

    @BindView(R.id.transmissionTextView)
    AppCompatTextView mTransmissionTextView;

    @BindView(R.id.carStatusTypeTextView)
    AppCompatTextView mStatusTypeTextView;

    @BindView(R.id.crudeTypeTextView)
    AppCompatTextView mCrudeTypeTextView;

    @BindView(R.id.layoutDetailsLinearLayout)
    LinearLayout mLayoutDetailsLinearLayout;

    @BindView(R.id.chevronDownImageView1)
    AppCompatImageView mChevronDownImageView1;

    @BindView(R.id.layoutInteriorLinearLayout)
    LinearLayout mLayoutInteriorLinearLayout;

    @BindView(R.id.chevronDownImageView)
    AppCompatImageView mChevronDownImageView;

    @BindView(R.id.RVInteriorHeaderList)
    RecyclerView mRVInteriorList;

    @BindView(R.id.layoutExteriorLinearLayout)
    LinearLayout mLayoutExteriorLinearLayout;

    @BindView(R.id.chevronDownImageView2)
    AppCompatImageView mChevronDownImageView2;

    @BindView(R.id.RVExteriorHeaderList)
    RecyclerView mRVExteriorList;

    @BindView(R.id.layoutEquipmentLinearLayout)
    LinearLayout mLayoutEquipmentLinearLayout;

    @BindView(R.id.chevronDownImageView3)
    AppCompatImageView mChevronDownImageView3;

    @BindView(R.id.RVEquipmentHeaderList)
    RecyclerView mRVEquipmentList;

    @BindView(R.id.descriptionTextView)
    ExpandableTextView mDescriptionTextView;

    @BindView(R.id.seeMoreTextView)
    AppCompatTextView mSeeMoreTextView;

    @OnClick(R.id.seeMoreTextView)
    void onSeeMoreClicked() {
        if (mDescriptionTextView.isExpanded()) {
            mDescriptionTextView.collapse();
            mSeeMoreTextView.setText("See Less");
            return;
        }
        mDescriptionTextView.expand();
        mSeeMoreTextView.setText("See More");
    }


    @OnClick(R.id.layoutDetailsRelativeLayout)
    void onDetailHeaderClicked() {
        if (mLayoutDetailsLinearLayout.getVisibility() == View.VISIBLE) {
            mLayoutDetailsLinearLayout.setVisibility(View.GONE);
            mChevronDownImageView.setImageResource(R.drawable.ic_arrow_down);
            return;
        }
        mLayoutDetailsLinearLayout.setVisibility(View.VISIBLE);
        mChevronDownImageView.setImageResource(R.drawable.ic_arrow_up);
    }

    @OnClick(R.id.layoutInteriorRelativeLayout)
    void onInteriorHeaderClicked() {
        if (mLayoutInteriorLinearLayout.getVisibility() == View.VISIBLE) {
            mLayoutInteriorLinearLayout.setVisibility(View.GONE);
            mChevronDownImageView1.setImageResource(R.drawable.ic_arrow_down);
            return;
        }
        mLayoutInteriorLinearLayout.setVisibility(View.VISIBLE);
        mChevronDownImageView1.setImageResource(R.drawable.ic_arrow_up);
    }

    @OnClick(R.id.layoutExteriorRelativeLayout)
    void onExteriorHeaderClicked() {
        if (mLayoutExteriorLinearLayout.getVisibility() == View.VISIBLE) {
            mLayoutExteriorLinearLayout.setVisibility(View.GONE);
            mChevronDownImageView2.setImageResource(R.drawable.ic_arrow_down);
            return;
        }
        mLayoutExteriorLinearLayout.setVisibility(View.VISIBLE);
        mChevronDownImageView2.setImageResource(R.drawable.ic_arrow_up);
    }

    @OnClick(R.id.layoutEquipmentRelativeLayout)
    void onEquipmentHeaderClicked() {
        if (mLayoutEquipmentLinearLayout.getVisibility() == View.VISIBLE) {
            mLayoutEquipmentLinearLayout.setVisibility(View.GONE);
            mChevronDownImageView3.setImageResource(R.drawable.ic_arrow_down);
            return;
        }
        mLayoutEquipmentLinearLayout.setVisibility(View.VISIBLE);
        mChevronDownImageView3.setImageResource(R.drawable.ic_arrow_up);
    }

    FullDetailAdapter mHeaderAdapter;
    FullDetailAdapter mValueAdapter;
    FullDetailAdapter mInteriorAdapter;
    FullDetailAdapter mExteriorAdapter;
    FullDetailAdapter mEquipmentAdapter;

    LinearLayoutManager llmHeaders;
    LinearLayoutManager llmValues;
    LinearLayoutManager llmInterior;
    LinearLayoutManager llmExterior;
    LinearLayoutManager llmEquipment;

    ArrayList<String> mHeaderDatas;
    ArrayList<String> mValueDatas;
    ArrayList<String> mInteriorValues;
    ArrayList<String> mExteriorValues;
    ArrayList<String> mEquipmentValues;

    Result mResultData;

    @Override
    protected BasePresenter injectPresenter() {
        return new CarDataPresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialize();
    }

    @Override
    public int setLayoutResource() {
        return R.layout.activity_car_details;
    }


    /**
     * Initializes all the views and variables
     */
    private void initialize() {
        setToolbar(true, mToolbar);
        llmHeaders = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        llmValues = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        llmInterior = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        llmExterior = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        llmEquipment = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mHeaderDatas = new ArrayList<>();
        mValueDatas = new ArrayList<>();
        mInteriorValues = new ArrayList<>();
        mExteriorValues = new ArrayList<>();
        mEquipmentValues = new ArrayList<>();
        mHeaderAdapter = new FullDetailAdapter(mContext, mHeaderDatas, true);
        mValueAdapter = new FullDetailAdapter(mContext, mValueDatas, false);
        mInteriorAdapter = new FullDetailAdapter(mContext, mInteriorValues, true);
        mExteriorAdapter = new FullDetailAdapter(mContext, mExteriorValues, true);
        mEquipmentAdapter = new FullDetailAdapter(mContext, mExteriorValues, true);
        mRVHeaderList.setAdapter(mHeaderAdapter);
        mRVValueList.setAdapter(mValueAdapter);
        mRVHeaderList.setLayoutManager(llmHeaders);
        mRVValueList.setLayoutManager(llmValues);
        mRVInteriorList.setAdapter(mInteriorAdapter);
        mRVInteriorList.setLayoutManager(llmInterior);
        mRVExteriorList.setAdapter(mExteriorAdapter);
        mRVExteriorList.setLayoutManager(llmExterior);
        mRVEquipmentList.setAdapter(mEquipmentAdapter);
        mRVEquipmentList.setLayoutManager(llmEquipment);
        collectExtra();
    }


    /**
     * Collects extra data passed from the list view on previous activity
     */
    private void collectExtra() {
        mResultData = (Result) getIntent().getSerializableExtra(SERIALIZED_RESULT_KEY);
        fillInData(mResultData);
    }

    /**
     * Fills the view with the collected data
     * @param result this data is from the previous activity that is passed through intent as serialized object
     */
    private void fillInData(Result result) {
        ImageLoader.loadMedia(mContext, mFullImageView, result.images.get(0).url);
        mCarTitleTextView.setText(result.data.name);
        mPriceTextView.setText(AppUtils.formattedNumber(result.data.price));
        mPriceConditionTextView.setText(result.data.attributes.priceConditions);
        mActivateAtTextView.setText("Submitted on: " + AppUtils.formatDate(result.data.activatedAt));
        mMileageValueTextView.setText(result.data.mileageNotAvailable.equalsIgnoreCase("1") ? "-" : result.data.mileage);
        mStatusTypeTextView.setText(result.data.condition);
        mTransmissionTextView.setText(result.data.transmission);
        mCrudeTypeTextView.setText(result.data.fuel);
        mDescriptionTextView.setText(result.data.attributes.description);
        addDataToHeaderFields(result);
        addDataToValueFields(result);
        addDataToInteriorHeader(result);
        addDataToExteriorHeader(result);
        addDataToEquipmentHeader(result);

    }

    /**
     * This methods are used to populate the details, interior, exterior and equipment
     * @param result
     */
    private void addDataToEquipmentHeader(Result result) {
        List<String> mOptionalData = result.data.attributes.all.optional.get(2).options;
        for (String string : mOptionalData) {
            mEquipmentValues.add(string);
        }
        mEquipmentAdapter.setDataset(mEquipmentValues);
    }

    private void addDataToExteriorHeader(Result result) {
        List<String> mOptionalData = result.data.attributes.all.optional.get(1).options;
        for (String string : mOptionalData) {
            mExteriorValues.add(string);
        }
        mExteriorAdapter.setDataset(mExteriorValues);
    }

    private void addDataToInteriorHeader(Result result) {
        List<String> mOptionalData = result.data.attributes.all.optional.get(0).options;
        for (String string : mOptionalData) {
            mInteriorValues.add(string);
        }
        mInteriorAdapter.setDataset(mInteriorValues);
    }

    private void addDataToHeaderFields(Result result) {
        List<Detail> mDetailHeader = result.data.attributes.all.details;
        for (Detail detail : mDetailHeader) {
            if (detail == mDetailHeader.get(0)) continue;
            if (detail.value.isEmpty()) continue;
            mHeaderDatas.add(detail.labelEn);
        }
        mHeaderAdapter.setDataset(mHeaderDatas);
    }

    private void addDataToValueFields(Result result) {
        List<Detail> mDetailHeader = result.data.attributes.all.details;
        for (Detail detail : mDetailHeader) {
            if (detail == mDetailHeader.get(0)) continue;
            if (detail.value.isEmpty()) continue;
            mValueDatas.add(detail.value);
        }
        mValueAdapter.setDataset(mValueDatas);
    }


    private ArrayList<String> getImageUrl(Result result) {
        ArrayList<String> imageUrls = new ArrayList<>();

        for (Image image : result.images) {
            imageUrls.add(image.url);
        }
        return imageUrls;
    }

    /**
     * Starts this activity
     * @param context from calling activity
     * @param result data from previous activity
     */
    public static void newActivity(Context context, Result result) {
        Intent i = new Intent(context, CarDetailsActivity.class);
        i.putExtra(SERIALIZED_RESULT_KEY, result);
        context.startActivity(i);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    @Override
    public void showLoadingDialog() {

    }

    @Override
    public void hideLoadingDialog() {

    }
}
