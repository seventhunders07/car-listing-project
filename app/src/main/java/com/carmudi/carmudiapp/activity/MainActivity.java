package com.carmudi.carmudiapp.activity;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ProgressBar;

import com.carmudi.carmudiapp.R;
import com.carmudi.carmudiapp.adapter.RVDataAdapter;
import com.carmudi.carmudiapp.adapter.SortOrderAdapter;
import com.carmudi.carmudiapp.interfaces.RecyclerViewItemClickListener;
import com.carmudi.carmudiapp.model.CarDataResponse;
import com.carmudi.carmudiapp.model.Result;
import com.carmudi.carmudiapp.model.SortOrderModel;
import com.carmudi.carmudiapp.presenter.BasePresenter;
import com.carmudi.carmudiapp.presenter.CarDataPresenter;
import com.carmudi.carmudiapp.utils.Preferences;
import com.carmudi.carmudiapp.view.CarDataView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class MainActivity extends BaseActivity implements CarDataView {

    public static String LIST_STATE_KEY = "listState";

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.RVDataList)
    RecyclerView mRVDataList;

    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @BindView(R.id.progressBar)
    ProgressBar mProgressBar;

    @BindView(R.id.bottomSheetLayout)
    ConstraintLayout mBottomSheetLayout;

    @BindView(R.id.sortOrderRecyclerView)
    RecyclerView mSortOrderRecyclerView;
    private Parcelable mListState;

    @OnClick(R.id.sortRelativeLayout)
    void onSortClicked() {
        if (mSheetBehavior.getState() == BottomSheetBehavior.STATE_COLLAPSED) {
            changeSheetBehavior(true);
            return;
        }
        changeSheetBehavior(false);
    }

    RVDataAdapter mAdapter;
    SortOrderAdapter mSortOrderAdapter;
    ArrayList<Result> mCarDataResult;
    CarDataPresenter mPresenter;
    LinearLayoutManager lm;
    boolean isLoadNew = true;
    public int pageNum = 1;
    static MainActivity instance;

    BottomSheetBehavior mSheetBehavior;
    private ArrayList<SortOrderModel> orderModels;

    /**
     * presenter attachment
     * @return new instance of presenter
     */
    @Override
    protected BasePresenter injectPresenter() {
        return new CarDataPresenter();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        instance = this;

        //restore saved presenter
        mPresenter = (CarDataPresenter) getLastCompositeCustomNonConfigurationInstance();
        if (mPresenter == null) {
            mPresenter = getPresenter(CarDataPresenter.class);
        }
        initialize();
    }

    /**
     * saves the state of recyclerview
     * @param outState
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mListState = lm.onSaveInstanceState();
    }

    /**
     * restores the state of saved states (e.g) recyclerview
     * @param savedInstanceState
     */
    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState != null)
            mListState = savedInstanceState.getParcelable(LIST_STATE_KEY);
    }

    /**
     * part of mobile life cycle
     * called after onCreate
     */
    @Override
    public void onResume() {
        super.onResume();
        if (mListState != null) {
            lm.onRestoreInstanceState(mListState);
        }
    }


    /**
     * saves the state of the presenter when orientation changes
     * @return
     */
    @Override
    public Object onRetainCompositeCustomNonConfigurationInstance() {
        return mPresenter;
    }

    private void initialize() {
        setToolbar(false, toolbar);
        mSheetBehavior = BottomSheetBehavior.from(mBottomSheetLayout);
        mCarDataResult = new ArrayList<>();
        mAdapter = new RVDataAdapter(mContext, mCarDataResult);
        lm = new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false);
        mRVDataList.setAdapter(mAdapter);
        mRVDataList.setLayoutManager(lm);

        orderModels = new ArrayList<>();
        mSortOrderAdapter = new SortOrderAdapter(mContext, orderModels);
        mSortOrderRecyclerView.setAdapter(mSortOrderAdapter);
        mSortOrderRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        checkIfSortApplied(false);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                checkIfSortApplied(false);
            }
        });
        mRVDataList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = lm.getChildCount();
                int totalItemCount = lm.getItemCount();
                int firstVisibleItemPosition = lm.findFirstVisibleItemPosition();

                // Load more if we have reach the end to the recyclerView
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount && firstVisibleItemPosition >= 0) {
                    if (!isLoadNew)
                        checkIfSortApplied(true);
                    mProgressBar.setVisibility(View.VISIBLE);
                    isLoadNew = false;
                }
            }
        });
        checkFirstTime();
        initializeBottomSheet();

    }

    /**
     * Initialization of bottom sheet layout
     */
    private void initializeBottomSheet() {
        mSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED: {
                    }
                    break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {

                    }
                    break;
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    /**
     * Initialize data of sort adapter
     */
    private void initializeData() {
        orderModels.add(new SortOrderModel("Oldest - Newest", "oldest", false));
        orderModels.add(new SortOrderModel("Newest - Oldest", "newest", false));
        orderModels.add(new SortOrderModel("Lowest Price - Highest Price", "price-low", false));
        orderModels.add(new SortOrderModel("Highest Price - Lowest Price", "price-high", false));
        orderModels.add(new SortOrderModel("Lowest Mileage - Highest Mileage", "mileage-low", false));
        orderModels.add(new SortOrderModel("Highest Mileage - Lowest Mileage", "mileage-high", false));
        mSortOrderAdapter.setDataset(orderModels);
        SortOrderModel.addAllToDatabase(orderModels);
        Preferences.setBoolean(this, "isFirstTime", true);
    }

    /**
     * checks if user used the app for the first time
     */
    private void checkFirstTime() {
        orderModels.clear();
        if (!Preferences.getBoolean(this, "isFirstTime")) {
            initializeData();
            return;
        }

        orderModels.addAll(SortOrderModel.getAllDataFromDatabase());
        mSortOrderAdapter.setDataset(orderModels);
    }

    /**
     * changes state of bottom sheet
     *
     * @param isExpand it tells the presenter that gets data to add the new data to array list or override data
     */
    public void changeSheetBehavior(boolean isExpand) {
        if (isExpand) {
            mSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            return;
        }
        mSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    /**
     * method for applying sort
     * @param isAddData tells the method to add or override data on arraylist
     */
    public void checkIfSortApplied(boolean isAddData) {
        ArrayList<SortOrderModel> orderModelArrayList = new ArrayList<>(SortOrderModel.getAllDataFromDatabase());
        for (SortOrderModel itemData : orderModelArrayList) {
            if (itemData.isItemSelected()) {
                mPresenter.getSortedCarData(MainActivity.this, pageNum, 10, itemData.getmSortOrderValue(), isAddData);
                return;
            }
        }
        mPresenter.getCarData(MainActivity.this, pageNum, 10, isAddData);
    }

    public static MainActivity getInstance() {
        return instance;
    }

    /**
     * sets layout resource to base activity
     * @return resource id of activity
     */
    @Override
    public int setLayoutResource() {
        return R.layout.activity_main;
    }


    @Override
    public void showLoadingDialog() {
    }

    @Override
    public void hideLoadingDialog() {

    }

    @Override
    public void onGetCarDataSuccess(CarDataResponse response, boolean isAddData) {
        pageNum++;
        if (pageNum < 10) {
            isLoadNew = true;
        }
        if (!isAddData) {
            mCarDataResult.clear();
        }
        mCarDataResult.addAll(response.metadata.results);
        mAdapter.setDataset(mCarDataResult);
        mSwipeRefreshLayout.setRefreshing(false);
        mProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onGetCarDataFailed(String error) {

    }
}
