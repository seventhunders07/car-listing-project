package com.carmudi.carmudiapp.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.carmudi.carmudiapp.R;
import com.carmudi.carmudiapp.interfaces.RVObservable;
import com.carmudi.carmudiapp.viewholder.RecyclerViewBaseViewHolder;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by root on 2/6/18.
 */

public class FullDetailAdapter extends RecyclerViewBaseAdapter {

    Context mContext;
    ArrayList<String> mData;
    boolean mIsBold;

    public FullDetailAdapter(Context context, ArrayList<String> data, boolean isBold) {
        mContext = context;
        mData = data;
        mIsBold = isBold;
    }

    @Override
    public RecyclerViewBaseViewHolder onCreateRecyclerViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.full_detail_item_layout, parent, false);
        return new FullDetailViewHolder(view, this);
    }

    @Override
    public void onBindRecyclerViewHolder(RecyclerViewBaseViewHolder holder, int position) {
        String itemString = mData.get(position);
        FullDetailViewHolder viewHolder = (FullDetailViewHolder) holder;
        viewHolder.mItemTextView.setText(itemString);
        if(mIsBold){
            viewHolder.mItemTextView.setTypeface(null, Typeface.BOLD);
            return;
        }
        viewHolder.mItemTextView.setTypeface(null, Typeface.NORMAL);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class FullDetailViewHolder extends RecyclerViewBaseViewHolder {
        @BindView(R.id.itemTextView)
        AppCompatTextView mItemTextView;

        protected FullDetailViewHolder(View itemView, RVObservable observable) {
            super(itemView, observable);
        }
    }
}
