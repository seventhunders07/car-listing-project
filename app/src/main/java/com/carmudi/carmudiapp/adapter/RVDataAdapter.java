package com.carmudi.carmudiapp.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.carmudi.carmudiapp.R;
import com.carmudi.carmudiapp.activity.CarDetailsActivity;
import com.carmudi.carmudiapp.interfaces.RVObservable;
import com.carmudi.carmudiapp.model.Result;
import com.carmudi.carmudiapp.utils.AppUtils;
import com.carmudi.carmudiapp.utils.ImageLoader;
import com.carmudi.carmudiapp.viewholder.RecyclerViewBaseViewHolder;

import java.util.ArrayList;

import butterknife.BindView;


public class RVDataAdapter extends RecyclerViewBaseAdapter {

    Context mContext;
    ArrayList<Result> mData;

    public RVDataAdapter(Context context, ArrayList<Result> data) {
        mContext = context;
        mData = data;
    }

    @Override
    public RecyclerViewBaseViewHolder onCreateRecyclerViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.layout_data_item, parent, false);
        return new RVDataViewHolder(view, this);
    }

    @Override
    public void onBindRecyclerViewHolder(RecyclerViewBaseViewHolder holder, int position) {
        final Result result = mData.get(position);
        RVDataViewHolder viewHolder = (RVDataViewHolder) holder;
        viewHolder.mCarTitleTextView.setText(result.data.name);
        viewHolder.mMileageValueTextView.setText(result.data.mileageNotAvailable.equalsIgnoreCase("1") ? "-" : result.data.mileage);
        viewHolder.mPriceTextView.setText(result.data.priceNotAvailable.equalsIgnoreCase("1") ? "No Price Available" : AppUtils.formattedNumber(result.data.price));
        viewHolder.mStatusTypeTextView.setText(result.data.condition);
        viewHolder.mTransmissionTextView.setText(result.data.transmission);
        viewHolder.mCrudeTypeTextView.setText(result.data.fuel);
        ImageLoader.loadMedia(mContext, viewHolder.mCarImageView, result.images.get(0).url);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CarDetailsActivity.newActivity(mContext, result);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class RVDataViewHolder extends RecyclerViewBaseViewHolder {

        @BindView(R.id.carImageView)
        AppCompatImageView mCarImageView;

        @BindView(R.id.carTitleTextView)
        AppCompatTextView mCarTitleTextView;

        @BindView(R.id.mileageValueTextView)
        AppCompatTextView mMileageValueTextView;

        @BindView(R.id.transmissionTextView)
        AppCompatTextView mTransmissionTextView;

        @BindView(R.id.carStatusTypeTextView)
        AppCompatTextView mStatusTypeTextView;

        @BindView(R.id.priceTextView)
        AppCompatTextView mPriceTextView;

        @BindView(R.id.crudeTypeTextView)
        AppCompatTextView mCrudeTypeTextView;


        protected RVDataViewHolder(View itemView, RVObservable observable) {
            super(itemView, observable);
        }
    }
}
