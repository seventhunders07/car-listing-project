package com.carmudi.carmudiapp.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.carmudi.carmudiapp.BaseApplication;
import com.carmudi.carmudiapp.R;
import com.carmudi.carmudiapp.activity.MainActivity;
import com.carmudi.carmudiapp.interfaces.RVObservable;
import com.carmudi.carmudiapp.model.SortOrderModel;
import com.carmudi.carmudiapp.viewholder.RecyclerViewBaseViewHolder;

import java.util.ArrayList;

import butterknife.BindView;

/**
 * Created by root on 2/6/18.
 */

public class SortOrderAdapter extends RecyclerViewBaseAdapter {

    Context mContext;
    ArrayList<SortOrderModel> mData;

    public SortOrderAdapter(Context context, ArrayList<SortOrderModel> data) {
        mContext = context;
        mData = data;
    }

    @Override
    public RecyclerViewBaseViewHolder onCreateRecyclerViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.sort_order_item_layout, parent, false);
        return new SortOrderViewHolder(view, this);
    }

    @Override
    public void onBindRecyclerViewHolder(RecyclerViewBaseViewHolder holder, int position) {
        final SortOrderModel sortOrderItem = mData.get(position);
        final SortOrderViewHolder viewHolder = (SortOrderViewHolder) holder;
        viewHolder.mTitleTextView.setText(sortOrderItem.getmSortOrderItem());
        viewHolder.mCheckImageView.setVisibility(sortOrderItem.isItemSelected() ? View.VISIBLE : View.GONE);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity.getInstance().changeSheetBehavior(false);
                unSelectOptions(sortOrderItem.id, viewHolder.mCheckImageView);
            }
        });
        if ((position + 1) > mData.size()) {
            viewHolder.mHorizontalView.setVisibility(View.GONE);
            return;
        }
        viewHolder.mHorizontalView.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public class SortOrderViewHolder extends RecyclerViewBaseViewHolder {
        @BindView(R.id.titleTextView)
        AppCompatTextView mTitleTextView;

        @BindView(R.id.checkImageView)
        AppCompatImageView mCheckImageView;

        @BindView(R.id.horizontalLineView)
        View mHorizontalView;

        protected SortOrderViewHolder(View itemView, RVObservable observable) {
            super(itemView, observable);
        }
    }

    private void unSelectOptions(long id, AppCompatImageView mCheckView) {
        for (SortOrderModel item : mData) {
            if(item.id == id){
                item.setIsItemSelected(!item.isItemSelected());
                mCheckView.setVisibility(View.VISIBLE);
            }else {
                item.setIsItemSelected(false);
                mCheckView.setVisibility(View.GONE);
            }
            SortOrderModel.updateDatabase((int) item.id, item.isItemSelected());
        }
        notifyDataSetChanged();
        MainActivity.getInstance().pageNum = 1;
        MainActivity.getInstance().checkIfSortApplied(false);
    }
}
