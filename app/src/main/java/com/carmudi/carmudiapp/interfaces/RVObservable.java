package com.carmudi.carmudiapp.interfaces;

public interface RVObservable {
    void registerObserver(RVObserver o);
    void notifyListenerAttached();
    void removeObserver();
}