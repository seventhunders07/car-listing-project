package com.carmudi.carmudiapp.interfaces;

public interface RVObserver {
    void update(RecyclerViewItemClickListener listener);
}