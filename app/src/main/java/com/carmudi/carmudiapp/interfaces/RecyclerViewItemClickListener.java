package com.carmudi.carmudiapp.interfaces;

import android.view.View;

public interface RecyclerViewItemClickListener<T> {
    void onClick(View view, T item, int position);
    boolean onLongClick(View view, T item, int position);
}