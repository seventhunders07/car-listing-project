package com.carmudi.carmudiapp.model;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class All implements Serializable{

@SerializedName("details")
@Expose
public List<Detail> details = null;
@SerializedName("optional")
@Expose
public List<Optional> optional = null;

}