package com.carmudi.carmudiapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Attributes {

@SerializedName("price_conditions")
@Expose
public String priceConditions;

}