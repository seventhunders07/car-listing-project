package com.carmudi.carmudiapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Attributes_ implements Serializable{

@SerializedName("description")
@Expose
public String description;
@SerializedName("year_built")
@Expose
public String yearBuilt;
@SerializedName("engine")
@Expose
public String engine;
@SerializedName("price_conditions")
@Expose
public String priceConditions;
@SerializedName("price_conditions_id")
@Expose
public String priceConditionsId;
@SerializedName("color_family")
@Expose
public String colorFamily;
@SerializedName("seats")
@Expose
public String seats;
@SerializedName("doors")
@Expose
public String doors;
@SerializedName("drive_type")
@Expose
public String driveType;
@SerializedName("warranty_type")
@Expose
public String warrantyType;
@SerializedName("warranty_years")
@Expose
public String warrantyYears;
@SerializedName("warranty_kms")
@Expose
public String warrantyKms;
@SerializedName("all")
@Expose
public All all;

}