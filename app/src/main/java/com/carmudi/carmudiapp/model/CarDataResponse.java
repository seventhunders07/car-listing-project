package com.carmudi.carmudiapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CarDataResponse {

@SerializedName("success")
@Expose
public Boolean success;
@SerializedName("messages")
@Expose
public Messages messages;
@SerializedName("session")
@Expose
public Session session;
@SerializedName("metadata")
@Expose
public Metadata metadata;

}