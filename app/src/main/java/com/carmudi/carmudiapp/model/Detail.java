package com.carmudi.carmudiapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Detail implements Serializable{

@SerializedName("name")
@Expose
public String name;
@SerializedName("label")
@Expose
public String label;
@SerializedName("label_en")
@Expose
public String labelEn;
@SerializedName("value")
@Expose
public String value;

}