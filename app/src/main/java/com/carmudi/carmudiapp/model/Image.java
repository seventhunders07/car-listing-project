package com.carmudi.carmudiapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Image implements Serializable {

@SerializedName("url")
@Expose
public String url;

}