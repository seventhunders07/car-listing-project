package com.carmudi.carmudiapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Meta {

@SerializedName("price_not_available")
@Expose
public String priceNotAvailable;
@SerializedName("original_price_currency")
@Expose
public String originalPriceCurrency;
@SerializedName("price")
@Expose
public String price;
@SerializedName("original_price")
@Expose
public String originalPrice;
@SerializedName("price_conditions_position")
@Expose
public String priceConditionsPosition;
@SerializedName("price_conditions_id")
@Expose
public String priceConditionsId;
@SerializedName("sku")
@Expose
public String sku;

}