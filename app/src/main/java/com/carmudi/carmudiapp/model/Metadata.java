package com.carmudi.carmudiapp.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Metadata {

@SerializedName("product_count")
@Expose
public String productCount;
@SerializedName("results")
@Expose
public List<Result> results = null;

}