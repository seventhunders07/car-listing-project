package com.carmudi.carmudiapp.model;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Optional implements Serializable{

@SerializedName("name")
@Expose
public String name;
@SerializedName("label")
@Expose
public String label;
@SerializedName("label_en")
@Expose
public String labelEn;
@SerializedName("options")
@Expose
public List<String> options = null;

}