package com.carmudi.carmudiapp.model;

import java.io.Serializable;
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Result implements Serializable{

@SerializedName("data")
@Expose
public Data data;
@SerializedName("id")
@Expose
public String id;
@SerializedName("images")
@Expose
public List<Image> images = null;

}