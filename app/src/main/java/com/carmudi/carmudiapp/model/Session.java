package com.carmudi.carmudiapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Session {

@SerializedName("id")
@Expose
public String id;
@SerializedName("expire")
@Expose
public String expire;
@SerializedName("YII_CSRF_TOKEN")
@Expose
public String yIICSRFTOKEN;

}
