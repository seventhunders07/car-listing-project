package com.carmudi.carmudiapp.model;

import android.util.Log;

import com.carmudi.carmudiapp.BaseApplication;

import java.util.ArrayList;
import java.util.List;

import io.objectbox.Box;
import io.objectbox.Property;
import io.objectbox.android.AndroidScheduler;
import io.objectbox.annotation.Entity;
import io.objectbox.annotation.Id;
import io.objectbox.reactive.DataObserver;
import io.objectbox.reactive.ErrorObserver;

/**
 * Created by root on 2/6/18.
 */

@Entity
public class SortOrderModel {

    @Id
    public long id;

    String mSortOrderItem;
    String mSortOrderValue;
    boolean mIsItemSelected;

    public SortOrderModel() {
    }

    public SortOrderModel(String mSortOrderItem, String mSortOrderValue, boolean mIsItemSelected) {
        this.mSortOrderItem = mSortOrderItem;
        this.mIsItemSelected = mIsItemSelected;
        this.mSortOrderValue = mSortOrderValue;
    }

    public String getmSortOrderItem() {
        return mSortOrderItem;
    }

    public String getmSortOrderValue() {
        return mSortOrderValue;
    }

    public boolean isItemSelected() {
        return mIsItemSelected;
    }

    public void setIsItemSelected(boolean isSelected) {
        this.mIsItemSelected = isSelected;
    }

    public static void updateDatabase(int id, boolean isSelected) {
        Box<SortOrderModel> box = BaseApplication.getInstance().getBoxStore().boxFor(SortOrderModel.class);
        SortOrderModel sortOrderModelBox = box.get(id);
        sortOrderModelBox.setIsItemSelected(isSelected);
        long updatedId = box.put(sortOrderModelBox);
    }

    public static void addAllToDatabase(ArrayList<SortOrderModel> data) {
        Box<SortOrderModel> box = BaseApplication.getInstance().getBoxStore().boxFor(SortOrderModel.class);
        box.put(data);
    }

    public static ArrayList<SortOrderModel> getAllDataFromDatabase() {
        Box<SortOrderModel> box = BaseApplication.getInstance().getBoxStore().boxFor(SortOrderModel.class);
        return new ArrayList<>(box.query().build().findLazyCached());
    }
}
