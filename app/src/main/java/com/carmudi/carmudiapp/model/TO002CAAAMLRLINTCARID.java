package com.carmudi.carmudiapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TO002CAAAMLRLINTCARID {

@SerializedName("meta")
@Expose
public Meta meta;
@SerializedName("attributes")
@Expose
public Attributes attributes;

}