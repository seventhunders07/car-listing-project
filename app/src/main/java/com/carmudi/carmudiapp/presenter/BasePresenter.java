package com.carmudi.carmudiapp.presenter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.carmudi.carmudiapp.view.BaseView;

import net.grandcentrix.thirtyinch.TiPresenter;

/**
 * Created by root on 2/5/18.
 */

public class BasePresenter extends TiPresenter<BaseView> {


    @Override
    protected void onAttachView(@NonNull BaseView view) {
        super.onAttachView(view);
    }

    @Nullable
    @Override
    public BaseView getView() {
        return super.getView();
    }
}
