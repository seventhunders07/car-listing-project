package com.carmudi.carmudiapp.presenter;

import android.util.Log;

import com.carmudi.carmudiapp.BaseApplication;
import com.carmudi.carmudiapp.model.CarDataResponse;
import com.carmudi.carmudiapp.rest.ApiService;
import com.carmudi.carmudiapp.rest.RestClient;
import com.carmudi.carmudiapp.view.CarDataView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by root on 2/5/18.
 */

public class CarDataPresenter extends BasePresenter {

    Call<CarDataResponse> carDataResponseCall;

    public CarDataPresenter() {
    }

    public void getCarData(final CarDataView mView, int pageNum, int maxCount, final boolean isAddData) {
        if (carDataResponseCall != null) {
            carDataResponseCall.cancel();
        }
        ApiService apiService = RestClient.create(BaseApplication.getInstance().getContext(), ApiService.class, false);
        carDataResponseCall = apiService.getCarData(pageNum, maxCount);
        carDataResponseCall.enqueue(new Callback<CarDataResponse>() {
            @Override
            public void onResponse(Call<CarDataResponse> call, Response<CarDataResponse> response) {
                if (!response.isSuccessful()) {
                    mView.onGetCarDataFailed("Something went wrong");
                    return;
                }

                mView.onGetCarDataSuccess(response.body(), isAddData);
            }

            @Override
            public void onFailure(Call<CarDataResponse> call, Throwable t) {
                Log.d("CarDataPresenter", "onFailure: " + t.getMessage());
            }
        });


    }



    public void getSortedCarData(final CarDataView mView, int pageNum, int maxCount, String sortOrder , final boolean isAddData) {
        if (carDataResponseCall != null) {
            carDataResponseCall.cancel();
        }
        ApiService apiService = RestClient.create(BaseApplication.getInstance().getContext(), ApiService.class, false);
        carDataResponseCall = apiService.getSortData(pageNum, maxCount, sortOrder);
        carDataResponseCall.enqueue(new Callback<CarDataResponse>() {
            @Override
            public void onResponse(Call<CarDataResponse> call, Response<CarDataResponse> response) {
                if (!response.isSuccessful()) {
                    mView.onGetCarDataFailed("Something went wrong");
                    return;
                }

                mView.onGetCarDataSuccess(response.body(), isAddData);
            }

            @Override
            public void onFailure(Call<CarDataResponse> call, Throwable t) {
                Log.d("CarDataPresenter", "onFailure: " + t.getMessage());
            }
        });


    }
}
