package com.carmudi.carmudiapp.rest;

import com.carmudi.carmudiapp.model.CarDataResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by root on 2/5/18.
 */

public interface ApiService {
    @GET("cars/page:{pageNum}/maxitems:{max}")
    Call<CarDataResponse> getCarData(@Path("pageNum") int pageNum, @Path("max") int max);

    @GET("cars/page:{pageNum}/maxitems:{max}/sort:{sortOrder}")
    Call<CarDataResponse> getSortData(@Path("pageNum") int pageNum, @Path("max") int max, @Path("sortOrder") String sortOrder);
}
