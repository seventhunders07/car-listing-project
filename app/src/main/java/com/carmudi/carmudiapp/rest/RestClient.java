
package com.carmudi.carmudiapp.rest;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Build.VERSION;
import android.provider.Settings.System;
import android.util.Log;
import android.widget.Toast;

import com.carmudi.carmudiapp.utils.Preferences;
import com.carmudi.carmudiapp.utils.RestUtils;
import com.carmudi.carmudiapp.utils.UiUtils;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class RestClient {

    private static String PREFS_CUSTOM_HEADERS = "PREFS_CUSTOM_HEADERS";
    private static String PREFS_INCLUDE_ADDITIONAL_HEADERS = "PREFS_INCLUDE_ADDITIONAL_HEADERS";
    private static Retrofit mRetrofit = null;

    public RestClient() {
    }

    public static Retrofit getRetrofitInstance() {
        return mRetrofit;
    }

    public static void includeAdditionalHeaders(Context context, boolean include) {
        Preferences.setBoolean(context, PREFS_INCLUDE_ADDITIONAL_HEADERS, include);
    }

    public static <T> T create(final Context context, Class<T> interFace, final boolean withAuthorization) {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        String baseUrl = RestUtils.getMetadataValue(context, "base-url");

        if (baseUrl.length() == 0) {
            UiUtils.showAlertDialog(context, "URL not found", "Base URL not found in manifest. Please declare a meta-data value with name \"base-url\".");
            return null;
        }

        OkHttpClient client = (new OkHttpClient.Builder())
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(180, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .addInterceptor(getHeadersInterceptor(context, withAuthorization, new HashMap<String, String>()))
                .build();

        mRetrofit = (new Retrofit.Builder())
                .client(client)
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create()).build();

        return mRetrofit.create(interFace);
    }

    public static <T> T create(final Context context, Class<T> interFace, final boolean withAuthorization, HashMap<String, String> customHeaders) {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);
        String baseUrl = RestUtils.getMetadataValue(context, "base-url");

        if (baseUrl.length() == 0) {
            Toast.makeText(context, "", Toast.LENGTH_SHORT).show();
            UiUtils.showAlertDialog(context, "URL not found", "Base URL not found in manifest. Please declare a meta-data value with name \"base-url\".");
            return null;
        }

        setAdditionalHeaders(customHeaders);

        OkHttpClient client = (new OkHttpClient.Builder())
                .addInterceptor(interceptor)
                .addInterceptor(getHeadersInterceptor(context, withAuthorization, customHeaders))
                .build();

        mRetrofit = (new Retrofit.Builder())
                .client(client)
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create()).build();

        return mRetrofit.create(interFace);
    }


    /**
     * HEADERS
     */

    private static Interceptor getHeadersInterceptor(final Context context, final boolean withAuthorization, final HashMap<String, String> customHeaders) {
        final String authKey = Preferences.getString(context, Preferences.API_KEY);
        final String fcmToken = Preferences.getString(context, Preferences.FCM_TOKEN);
//        final String appVersionName = BuildConfig.VERSION_NAME;
        final String osVersion = VERSION.RELEASE;

        Log.d("authkey", "token " + authKey);
        Log.d("authkey", "fcmkey " + fcmToken);

        return new Interceptor() {

            public Response intercept(Chain chain) throws IOException {

                // Getting App Version Name
                String appVersionName;
                try{
                    appVersionName = context.getPackageManager().getPackageInfo(context.getPackageName(),0).versionName;
                }catch (PackageManager.NameNotFoundException e){
                    appVersionName = "";
                    e.printStackTrace();
                }

                Request.Builder builder = chain.request().newBuilder()
                        .addHeader("X-Device-App-Version", appVersionName)
                        .addHeader("X-Device-OS", "android")
                        .addHeader("X-Device-OS-Version", osVersion)
                        .addHeader("X-Device-Manufacturer", "" + Build.MANUFACTURER)
                        .addHeader("X-Device-Model", "" + Build.MODEL)
                        .addHeader("X-Device-UDID", System.getString(context.getContentResolver(), "android_id"))
                        .addHeader("X-Device-FCM-Token", fcmToken);

                HashMap<String, String> headers = new HashMap<>(getCustomHeaders(customHeaders));
                if (customHeaders != null) {
                    headers.putAll(customHeaders);
                }

                if (includeCustomHeaders(context)) {
                    for (Map.Entry<String, String> entry : headers.entrySet()) {
                        String key = entry.getKey();
                        String value = entry.getValue();
                        builder.addHeader(key, value);
                    }
                }

                if (withAuthorization) {
                    builder.addHeader("Authorization", "" + authKey);
                }

                Request request = builder.build();
                return chain.proceed(request);
            }
        };
    }

    private static boolean includeCustomHeaders(Context context) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getPackageName(), 0);
        return sharedPreferences.getBoolean(PREFS_INCLUDE_ADDITIONAL_HEADERS, true);
    }

    // Extract custom headers
    public static String setAdditionalHeaders(HashMap<String, String> hash) {

        if (hash == null) {
            return "";
        }

        JsonObject jsonObjectBase = new JsonObject();
        JsonArray jsonArrayHeaders = new JsonArray();

        for (Map.Entry<String, String> entry : hash.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();

            JsonObject jsonObjectHeader = new JsonObject();
            jsonObjectHeader.addProperty("key", key);
            jsonObjectHeader.addProperty("value", value);
            jsonArrayHeaders.add(jsonObjectHeader);
        }

        jsonObjectBase.add("headers", jsonArrayHeaders);

//        Preferences.setString(context, PREFS_CUSTOM_HEADERS, jsonObjectBase + "");
        return jsonObjectBase.toString();
    }


    private static HashMap<String, String> getCustomHeaders(HashMap<String, String> headers) {

        String jsonPrefs = setAdditionalHeaders(headers);

        if (jsonPrefs.isEmpty() || jsonPrefs.contains("{}"))
            return headers;

        JsonObject jsonObjectPrefs = new JsonParser().parse(jsonPrefs).getAsJsonObject();
        JsonArray jsonArray = jsonObjectPrefs.get("headers").getAsJsonArray();
        for (int i = 0; i < jsonArray.size(); i++) {
            JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
            headers.put(jsonObject.get("key").getAsString(),
                    jsonObject.get("value").getAsString());
        }

        return headers;
    }
}
