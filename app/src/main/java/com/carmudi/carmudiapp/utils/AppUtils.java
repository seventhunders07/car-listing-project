package com.carmudi.carmudiapp.utils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by root on 2/5/18.
 */

public class AppUtils {

    public static String formattedNumber(String number) {
        double amount = Double.parseDouble(number);
        DecimalFormat formatter = new DecimalFormat("#,###.00");
        return "Rp " + formatter.format(amount);
    }

    public static String formatDate(String date) {
        SimpleDateFormat inputSDF = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
        try {
            Date inputDate = inputSDF.parse(date);
            SimpleDateFormat outputSDF = new SimpleDateFormat("yyyy-mm-dd");
            return outputSDF.format(inputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }
}
