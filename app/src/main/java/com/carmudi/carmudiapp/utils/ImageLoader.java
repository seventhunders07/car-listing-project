package com.carmudi.carmudiapp.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.annotation.Nullable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;
import com.carmudi.carmudiapp.R;

public class ImageLoader {

    public static void loadMedia(Context context, final ImageView imageView, String imageUrl) {
        RequestOptions options = new RequestOptions();
        options.centerCrop();
        options.dontAnimate();

        Glide.with(context)
                .asBitmap()
                .load(imageUrl)
                .apply(options)
                .into(imageView);
    }

    public static void loadProfilePicture(Context context, final ImageView imageView, String imageUrl) {
        RequestOptions options = new RequestOptions();
        options.centerCrop();
        Glide.with(context)
                .asBitmap()
                .load(imageUrl)
//                .placeholder(R.drawable.img_no_profile_pic)
                .apply(options)
                .into(imageView);
    }

    public static void loadMediaFitCenter(Context context, final ImageView imageView, String imageUrl) {
        RequestOptions options = new RequestOptions();
        options.fitCenter();
        Glide.with(context)
                .asBitmap()
                .load(imageUrl)
                .apply(options)
                .into(imageView);
    }

    public static void loadMediaFromDrawableFitCenter(Context context, final ImageView imageView, int drawable) {
        RequestOptions options = new RequestOptions();
        options.fitCenter();
        Glide.with(context)
                .asBitmap()
                .load(drawable)
                .apply(options)
                .into(imageView);
    }

    public static void loadMediaFromDrawable(Context context, final ImageView imageView, final Integer resourceId) {
        RequestOptions options = new RequestOptions();
        options.fitCenter();
        Glide.with(context)
                .asBitmap()
                .load(resourceId)
                .apply(options)
                .into(imageView);
    }

    public static void loadMediaCircular(final Context context, final ImageView imageView, String url){
        RequestOptions options = new RequestOptions();
        options.centerCrop();
        Glide.with(context).asBitmap().load(url).apply(options).into(new BitmapImageViewTarget(imageView) {
            @Override
            protected void setResource(Bitmap resource) {
                RoundedBitmapDrawable circularBitmapDrawable =
                        RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                circularBitmapDrawable.setCircular(true);
                imageView.setImageDrawable(circularBitmapDrawable);
            }
        });
    }
}
