package com.carmudi.carmudiapp.utils;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;


public class RestUtils {

    public static String getMetadataValue(Context context, String key) {
        ApplicationInfo app = null;
        try {
            app = context.getPackageManager().getApplicationInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            Bundle bundle = app.metaData;
            return bundle.getString(key, "");
        } catch (Exception var4) {
            var4.printStackTrace();
            return "";
        }
    }
}
