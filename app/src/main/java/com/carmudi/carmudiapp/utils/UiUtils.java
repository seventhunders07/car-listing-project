package com.carmudi.carmudiapp.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;


public class UiUtils {

    public UiUtils() {
    }

    public static void showAlertDialog(AlertDialog alertDialog) {
        dismissAlertDialog(alertDialog);
        alertDialog.show();
    }

    public static void showAlertDialog(Context context, String title, String message, boolean cancelable) {
        if(context != null) {
            if(!((Activity)context).isFinishing()) {
                AlertDialog alertDialog = (new AlertDialog.Builder(context)).setTitle(title).setMessage(message).setCancelable(cancelable).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                    }
                }).create();
                showAlertDialog(alertDialog);
            }
        }
    }

    public static void showAlertDialog(final Context context, String title, String message, boolean isCancelable, boolean finishActivity) {
        if(context != null) {
            if(!((Activity)context).isFinishing()) {
                AlertDialog alertDialog = (new AlertDialog.Builder(context)).setTitle(title).setMessage(message).setCancelable(isCancelable).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialogInterface, int i) {
                        ((Activity)context).finish();
                    }
                }).create();
                showAlertDialog(alertDialog);
            }
        }
    }

    public static void showAlertDialog(Context context, String title, String message, boolean cancelable, DialogInterface.OnClickListener onOkClickListener) {
        if(context != null) {
            if(!((Activity)context).isFinishing()) {
                AlertDialog alertDialog = (new AlertDialog.Builder(context)).setTitle(title).setMessage(message).setCancelable(cancelable).setPositiveButton("OK", onOkClickListener).create();
                showAlertDialog(alertDialog);
            }
        }
    }

    public static void showAlertDialog(Context context, String title, String message, boolean cancelable, String positiveText, DialogInterface.OnClickListener onPositiveClickListener, String negativeText, DialogInterface.OnClickListener onNegativeClickListener) {
        if(context != null) {
            if(!((Activity)context).isFinishing()) {
                AlertDialog alertDialog = (new AlertDialog.Builder(context)).setTitle(title).setMessage(message).setCancelable(cancelable).setPositiveButton(positiveText, onPositiveClickListener).setNegativeButton(negativeText, onNegativeClickListener).create();
                showAlertDialog(alertDialog);
            }
        }
    }

    public static void showAlertDialog(Context context, String title, String message, boolean cancelable, String positiveText, DialogInterface.OnClickListener onPositiveClickListener, String negativeText, DialogInterface.OnClickListener onNegativeClickListener, String neutralText, DialogInterface.OnClickListener onNeutralClickListener) {
        if(context != null) {
            if(!((Activity)context).isFinishing()) {
                AlertDialog alertDialog = (new AlertDialog.Builder(context)).setTitle(title).setMessage(message).setCancelable(cancelable).setPositiveButton(positiveText, onPositiveClickListener).setNegativeButton(negativeText, onNegativeClickListener).setNeutralButton(neutralText, onNeutralClickListener).create();
                showAlertDialog(alertDialog);
            }
        }
    }

    public static void showAlertDialog(Context context, String title, String message) {
        showAlertDialog(context, title, message, false);
    }

    public static void showAlertDialog(Context context, String message) {
        showAlertDialog(context, "", message, false);
    }

    public static void dismissAlertDialog(AlertDialog alertDialog) {
        if(alertDialog != null && alertDialog.isShowing()) {
            alertDialog.dismiss();
        }

        alertDialog = null;
    }

    public static void showItemsSelectionDialog(Context context, String[] items, DialogInterface.OnClickListener onClickListener) {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        alertDialog.setItems(items, onClickListener);
        alertDialog.create().show();
    }

    public static ProgressDialog defaultProgressDialog(Context context, String message) {
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(message);
        progressDialog.setCancelable(false);
        return progressDialog;
    }
}
