package com.carmudi.carmudiapp.view;

import net.grandcentrix.thirtyinch.TiView;
import net.grandcentrix.thirtyinch.callonmainthread.CallOnMainThread;

/**
 * Created by root on 2/5/18.
 */

public interface BaseView extends TiView {

    @CallOnMainThread
    void showLoadingDialog();

    @CallOnMainThread
    void hideLoadingDialog();


}
