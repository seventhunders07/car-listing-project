package com.carmudi.carmudiapp.view;

import com.carmudi.carmudiapp.model.CarDataResponse;

import net.grandcentrix.thirtyinch.callonmainthread.CallOnMainThread;

/**
 * Created by root on 2/5/18.
 */

public interface CarDataView extends BaseView {

    @CallOnMainThread
    void onGetCarDataSuccess(CarDataResponse response, boolean isAddData);

    @CallOnMainThread
    void onGetCarDataFailed(String error);
}
